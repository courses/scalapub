package ch.hepia.tpscala.result


sealed trait Result[+E,+A] {

  def map[B]( f: A=>B ): Result[E,B] = ???

  //F >: E, signifie "F est un supertype de E"
  def flatMap[B, F >:E]( f: A=>Result[F,B] ): Result[F,B] = ???

  //Retourne le résultat si OK
  //transforme l'erreur en une valeur valide sinon
  //B >: A signifie "B est un supertype de A
  //Une seul implémentation possible !
  def get[B >: A]( f: E => B ): B = ???

  //Une seul implémentation possible !
  def fold[B]( f: E => B, g: A => B ): B = ???

  def toOption: Option[A] = ???

}
case class OK[A]( value: A ) extends Result[Nothing,A]
case class Err[E]( value: E ) extends Result[E,Nothing]
object Result {
  //Si il y a au moins une erreur dans la liste, le résultat est une erreur
  //Si il n'y a que des succès, liste les succès
  def sequence[E,A]( res: List[Result[E,A]] ): Result[E,List[A]] = ???
}



case class Config( hostname: String, port: Int )
object Config {
  private def line2keyValue( line: String ): Result[String,(String,String)] = {
    val elements = line.split("=")
    if( elements.size != 2 ) Err("Syntax error: " + line )
    else OK( (elements(0), elements(1) ) )
  }

  private def lines2map( lines: List[String] ): Result[String,Map[String,String]] = 
    Result.sequence( lines.map(line2keyValue) ).map( _.toMap )

  //Utilisez lines2map pour parvenir au résultat
  //Attention au clés manquantes
  def parse( lines: List[String] ): Result[String,Config] = ???
}
