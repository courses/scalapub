Application de la programmation fonctionnelle
=============================================

Principe général
----------------

Appliquer les principes vus en cours pour améliorer vos codes de projet
de semestre, travail de bachelor, gros TP, etc.

L'idée est de trouver des points susceptible d'être améliorer et
d'envisager des alternaives issues de la programmation fonctionnelle,
pas forcément de les intégrer dans votre code.

Organisation
------------

-   Vous êtes sensés travailler en *groupes de deux*. Si cela s'avérait
    impossible, vous pouvez travailler seul.
-   La note est attribuée pour l'ensemble du groupe.
-   Les exemples présentés sont tirés des travaux des
    participants. Vous pouvez tout illustrer par le même travail ou
    alors illustrer chaque partie par un travail différent.
-   Le travail doit être impérativement rendu le 25 avril, mais vous
    devez faire le gros du travail jeudi matin à la place du cours.
-   Un PDF du rapport sera déposé sur CyberLearn
-   Si tout le code ne tiens pas dans le PDF, vous pouvez inclure l'URL
    d'un dépôt GIT.
-   Vous avez le droit de copier/coller du contenu de votre rapport de
    projet de semestre ou d'un TP

Consignes du travail
--------------------

1.  Votre travail devra comporter deux parties différentes, décrivant
    l'application de techniques vues en cours. Par exemple:
    -   Supression de répétions par fonctions anonymes
    -   Gestion du flux des erreurs avec des types avancés
    -   Utilisation des collections avec `map`, `flatMap`, `filter`, etc.

2.  Chaque partie sera introduire par un **petit** paragraphe expliquant
    le contexte du travail.
3.  Chaque partie sera construite autour d'un **extrait** de code servant
    d'illustration.
4.  Un texte **bref et concis** expliquera la techniques mise en oeuvre
    et les améliorations apportée. Vous pouvez par exemple montrer le
    code avant et après.
5.  Concentrez vous sur des extraits, ne mettez surtout pas tout le
    code.
6.  Chaque partie devra faire entre une et quatre pages **code
    compris**.
7.  Je ne veux pas voir d'introduction générale, ni de conclusion !
    Juste l'essentiel...
