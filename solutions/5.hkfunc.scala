package ch.hepia.tpscala

/* Implémentez les fonctions suivantes. Vous ne pouvez utiliser que les
 *  méthode de 'List' vues dans les exercices précédents.
 */
object HKFunc {

  /* La fonction 'map' applique une fonction 'f' sur chaque élément de
   * la liste 'as'.  La liste résultat doit avoir la même longueur que
   * l'argument.
   */
   def map[A,B]( as: List[A] )( f: A=>B ): List[B] = {
    def loop(rem: List[A], result:List[B]): List[B] = rem match {
      case Nil => result.reverse
      case a :: tail => loop( tail, f(a)::result )
    }
    loop( as, Nil )
  }

  /* La fonction 'filter' utilise le prédicat 'f' pour déterminer quel
   * élément garder. Le résultat peut être vide, mais l'ordre doit
   * être préservé.
   */
  def filter[A]( as: List[A] )( f: A=>Boolean ): List[A] = {
    def loop( rem: List[A], result: List[A] ): List[A] = rem match {
      case Nil => result.reverse
      case a :: tail if f(a) => loop( tail, a :: result )
      case _ :: tail => loop( tail, result )
    }
    loop(as, Nil)
  }
   
  /* Réduit une liste 'as' en utilisant une opération binaire 'f'.  On
   * supposera que 'as' n'est pas vide.
   */
  def reduce[A]( as: List[A] )( f: (A,A)=>A ): A = {
    def loop( rem: List[A], result: A ):A = rem match {
      case Nil => result
      case a :: tail => loop( tail, f(result,a) )
    }
    loop(as.tail,as.head)
  }

  /* Transforme une fonction 'f' en une fonction s'appliquant sur une
  *  liste. Utiliser la fonction 'map' définie ci-dessus
  */
  def lift[A,B]( f: A=>B ): List[A]=>List[B] =
    (lst:List[A]) => map( lst)( f )


  /* DIFFICILE. Transforme une liste 'as' au moyen de la fonction 'f'.
   * Cette fonction est appliquée à chaque élément de 'as' pour
   * produire une nouvelle liste (qui peut être vide). Le résultat est
   * la concaténation de chaque nouvelle liste en respectant l'ordre.
   */
  def bind[A,B]( as: List[A] )( f: A=>List[B] ): List[B] = {
    def loop( rem: List[A], result:List[B]): List[B] = rem match {
      case Nil => result
      case a :: tail => loop( tail, result ++ f(a) )
    }
    loop(as, Nil)
  }

  def bind2[A,B]( as: List[A] )( f: A=>List[B] ): List[B] =
    if( as.isEmpty ) Nil
    else {
      val bs: List[List[B]] = map( as )( f )
      reduce( bs )( (x,y) => x ++ y ) 
    }

  def flat[A]( as: List[List[A]] ): List[A] =
    if( as.isEmpty ) Nil
    else reduce(as)( _ ++ _ )

  def bind3[A,B]( as: List[A] )( f: A=>List[B] ): List[B] = {
    flat( map( as )( f ) )
  } 

  def map_[A,B]( as: List[A] )( f: A=>B ): List[B] =
    bind(as)( f andThen (b => List(b)) )

  def filter_[A]( as: List[A] )( f: A=>Boolean ): List[A] =
    bind(as)( a => if( f(a) ) List(a) else Nil )

}
