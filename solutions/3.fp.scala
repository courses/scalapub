
/*
 * Implémenter les fonctions suivantes en suivant les commentaires.
 * Respectez également les consignes suivantes:
 *  - Toutes les fonctions doivent être pures
 *  - Tout doit être immutable (val au lieu de var)
 *  - Utiliser la recursion terminale si possible
 *  - Utiliser le pattern matching si possible
 */
object Serie3 {

  /*
   * Donne la longueur d'une liste. Votre implémentation ne peut
   *  utiliser aucune fonction de List excepté isEmpty()
   */
  def len[A]( as: List[A] ): Int =  {
    def lenRec( rest: List[A], size: Int ): Int = rest match {
      case Nil => size
      case _ :: rest2 => lenRec( rest2, size + 1)
    }
   lenRec(as, 0)
  }

  /*
   * Inverse l'ordre des éléments d'une liste. Votre implémentation ne peut
   * utiliser aucune fonction de List excepté:
   *    - isEmpty
   *    - ::
   *    - head
   *    - tail
   */
  def rev[A]( as: List[A] ): List[A] = {
    def revRec( rest: List[A], out: List[A] ): List[A] =
      rest match {
        case Nil => out
        case  a :: b => revRec( b, a :: out )
      }
    revRec( as, Nil )
  }

  /*
   * Somme les éléments d'une liste. Votre implémentation ne peut
   * utiliser aucune fonction de List excepté: -
   *    - isEmpty
   *    - head
   *    - tail
   */
  def sum( xs: List[Int] ): Int =  {
    def sumRec( rest: List[Int], n: Int ): Int =
      rest match {
        case Nil => n
        case i :: is => sumRec( is, n + i )
      }
    sumRec(xs, 0)
  }

  /*
   *  Retourne vrai si et seulement si la liste xs ne
   *  comprend que des valeures vraies. Votre implémentation 
   *  ne peutcutiliser aucune fonction de List excepté:
   *    - isEmpty
   *    - head
   *    - tail
   */
  // Le mot clé final permet d'activer la recursion terminale
  //  (alternative aux méthodes imbriquées)
  final def and( xs: List[Boolean] ): Boolean = xs match {
    case Nil => true
    case false :: _ => false
    case _ :: rest => and(rest)
  }

  

  /*
   *  Applatit une liste. Votre implémentation 
   *  ne peut utiliser aucune fonction de List excepté:
   *   - isEmpty
   *   - head
   *   - tail
   *   - ++
   */
  type LL[X] = List[List[X]] //Alias de type, permet d'alléger la syntaxe
  def flat[A]( las: LL[A] ): List[A] = {
    def flatRec( rest: LL[A], out: List[A] ): List[A] = rest match {
      case Nil => out
      case a :: b => flatRec(b, rev(a) ++ out )
    }
    rev( flatRec(las,Nil) )
  }


  /*
   *  Retourne vrai si la Liste a une nombre pair d'éléments. Votre
   *  implémentation ne peut utiliser aucune fonction de List !  Vous
   *  devez utiliser le pattern matching.
   */
  final def even[A]( as: List[A] ): Boolean = as match {
    case Nil => true
    case _ :: Nil => false
    case _ :: _ :: rest => even(rest)
  }

}

object Serie3Main extends App {

  import Serie3._

  val is = List( 1, 2, 3, 4, 5 )
  val bs1 = List( true, true, false, true )
  val bs2 = List( true, true, true )
  val las1 = List.empty[List[Int]]
  val las2 = List( List(1,2), List(3), List(4,5) )

  require( rev(is) == List( 5, 4, 3, 2, 1 ) )

  require( len(is) == 5 )
  require( len( las1 ) == 0 )
  require( len( bs1 ) == 4 )

  require( flat(las1) == Nil )
  require( flat(las2) == is )

  require( sum(is) == 15 )
  require( sum(flat(las2)) == sum(is) )

  require( rev( bs1 ) == List( true, false, true, true ) )
  require( rev( bs2 ) == bs2 )

  require( even( is ) == false )
  require( even( bs1 ) == true )
  require( even( las1 ) == true )

}
