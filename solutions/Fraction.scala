package ch.unige.hepia.tp


case class Fraction private (num: Long, den: Long) extends Ordered[Fraction]{
  def invert = new Fraction(den,num)
  def +( that: Fraction ): Fraction = {
    val n = num*that.den + that.num*den
    val d = that.den*den
    Fraction.simplify(n,d)
  }
  def -( that: Fraction ): Fraction =
    this + (-that)
  def *(that: Fraction ): Fraction = 
    Fraction.simplify( num*that.num, den*that.den )
  def /(that: Fraction ): Fraction =
    this * that.invert
  def unary_- = new Fraction(-num, den)

  def compare(that: Fraction): Int =
    ( num*that.den ) compare ( that.num*den )

  def toDouble = num.toDouble / den

  override def toString = s"$num\\$den"
}

object Fraction {

  def simplify( num: Long, den: Long ): Fraction = {
    if( num <0 && den < 0 )
      simplify( -num, -den )
    else {
      val n = gcd(num,den)
      new Fraction( num/n, den/n)
    }
  }

  /*
   fromDouble( 0.123456, 1 ) => 1\10
   fromDouble( 0.123456, 2 ) => 3\25 (12\100)
   fromDouble( 0.123456, 3 ) => 123\1000
   */
  def fromDouble( x: Double, prec: Int ): Fraction = {
    val den = math.pow(10,prec).toLong
    val num = (x * den).round
    Fraction.simplify( num, den )
  }

  private def gcd( i: Long, j: Long ): Long =
    if( j == 0 )
      i
    else
      gcd( j, i % j )



}


