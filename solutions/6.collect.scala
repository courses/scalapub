package ch.hepia.tpscala


/* Implémentez les fonctions suivantes. 
 */
object Collect {

  case class Album( title: String, artist: String, year: Int )
  case class Duration( minutes: Int, seconds: Int ) {
    def toSeconds = minutes*60 + seconds
  }
  object Duration {
    def fromSeconds( sec: Int ) = Duration( sec / 60, sec % 60 )
  }
  case class Track( title: String, duration: Duration )

  val albums = List(
    Album( "Mit Gas", "Tomahawk", 2003 ),
    Album( "Pork Soda", "Primus", 1993 ),
    Album( "Brown Album", "Primus", 1997 ),
    Album( "Distraction Pieces", "Scroobius Pip", 2011 )
  )

  val tracks = Map(  //Map[String,List[Track]] <: (String)=>List[Track]
    "Mit Gas" -> List(
      Track( "Mayday", Duration( 3, 32 ) )
    ),
    "Pork Soda" -> List(
      Track( "DMV", Duration( 4, 58 ) ),
      Track( "Mr. Krinkle", Duration( 5, 27 ) )
    ),
    "Brown Album" -> List(
      Track( "Fisticuffs", Duration( 4, 25 ) ),
      Track( "Camelback Cinema", Duration( 4, 0 ) ),
      Track( "Kalamazoo", Duration( 3, 31 ) )
    ),
    "Distraction Pieces" -> List(
      Track( "Let 'Em Come", Duration( 4, 25 ) ),
      Track( "Domestic Silence", Duration( 3, 58 ) )
    )
  )


  /* Retourne la liste de morceaux associés à un artiste */
  def tracksOf0( artist: String ): List[Track] =
    albums.filter( _.artist == artist ) //: List[Album]
     .map( _.title ) //: List[String]
     .flatMap( tracks ) //: List[Track]

  def tracksOf1( artist: String ): List[Track] =
    albums.flatMap{ (a:Album) =>
      if( a.artist == artist ) tracks(a.title)
      else Nil
    }

  def tracksOf( artist: String ): List[Track] =
    for {
      alb   <- albums if alb.artist == artist
      trcks <- tracks(alb.title)
    } yield trcks


  /* Retourne la liste de tous les morceaux de moins de 4 minutes */
  def shortTracks0: List[Track] =
    tracks.values.toList.flatten.filter( _.duration.toSeconds < 4*60 )

  /* Retourne la liste de tous les morceaux de moins de 4 minutes */
  def shortTracks1: List[Track] =
    tracks.toList.flatMap{
      case (_,trcks) => trcks.filter( _.duration.toSeconds < 4*60 )
    }

  def shortTracks: List[Track] =
    for {
      (_,trcks) <- tracks.toList
      track <- trcks if track.duration.toSeconds < 4*60
    } yield track


  /* Retourne les titres des morceaux antérieurs à une année */
  def titlesBefore1( year: Int ): List[String] =
    for {
      alb <- albums if alb.year < year
      track <- tracks(alb.title)
    } yield track.title

  def titlesBefore( year: Int ): List[String] =
    albums.flatMap { alb =>
      if( alb.year < year ) tracks(alb.title).map(_.title)
      else Nil
    }

  /* Calcule la durée totale de tous les morceaux disponibles.
     REMARQUE: ont veut que les secondes soient inférieures à 60 mais les
     minutes peuvent dépasser ce total.
   */
  def totalDuration: Duration = {
    val total = tracks.values.toList.flatten
      .map( _.duration.toSeconds ).sum
    Duration.fromSeconds(total)
  }


}
